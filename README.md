# Sensortechnik in der Rohstoffwirtschaft

[![RWTHjupyter](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/)

This repository contains examples and exercises for the lecture "Sensortechnik in der Rohstoffwirtschaft" at RWTH Aachen University, held by the [Chair of Anthropogenic Material Cycles](http://www.ants.rwth-aachen.de) and the [Unit of Mineral Processing](http://www.amr.rwth-aachen.de).

## Quick Start

You can run the provided notebook by using [![RWTHjupyter](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/). 

## Local Usage

### 1. Clone this repository to your local machine.

If you want to operate the provided examples and excercises at your local machine you to run the run the following command in the terminal or git bash programm:

```bash
git clone https://git.rwth-aachen.de/tabea.scherling1/sir.git
```

### 2. Install Anaconda

In the next step you need to install the required packages. We recommend using [Anaconda](https://docs.anaconda.com/free/anaconda/install/index.html) therefore. Please follow the instructions on the website to install Anaconda on your local machine.

### 3. Install the provided environment

Than you can create a new environment with the required packages by running the following command in the terminal:

```bash
conda env create -f environment.yml
```

Than you also need to activate the environment by running the following command:

```bash
conda activate environment
```

## License

This notebook is provided under the MIT License. Please assign the work as follows:

Nils Kroell, Tabea Scherling, Beispiele und Übungen aus "Sensortechnik in der Rohstoffwirtschaft", 2023, Chair of Anthropogenic Material Cycles, Unit of Mineral Processing, RWTH Aachen University.